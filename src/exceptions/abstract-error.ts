
export abstract class AbstractError extends Error {
    public abstract readonly type: string;
}