import { AbstractError } from "./abstract-error";

export class InvalidArgumentException extends AbstractError {
    public get type(): string {
        return 'InvalidArgumentException';
    }
}