import { AbstractError } from "./abstract-error";

export class NullPointerException extends AbstractError {
    public get type(): string {
        return 'NullPointerException';
    }
}