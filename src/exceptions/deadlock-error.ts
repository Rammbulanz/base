import { AbstractError } from "./abstract-error";

export class DeadlockError extends AbstractError {
    public get type(): string {
        return 'DeadlockError';
    }
}