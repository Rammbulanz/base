import { AbstractError } from "./abstract-error";

export class IndexOutOfRangeException<T = any> extends AbstractError {

    public get type(): string {
        return 'IndexOutOfRangeException'
    }

    public readonly collection: T;

    constructor(collection: T, message: string) {
        super(message);
        this.collection = collection;
    }

}