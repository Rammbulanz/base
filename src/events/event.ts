
export interface EventInit<T extends EventTarget> {
    isTrusted?: boolean;
    target: T;
}

export class Event<T extends EventTarget> {

    readonly type: string;

    readonly isTrusted: boolean;
    readonly target: T

    constructor(type: string, init?: EventInit<T>) {

        this.type = Object.freeze(type);
        
        this.isTrusted = Object.freeze(typeof init.isTrusted === "boolean" ? init.isTrusted : false);
        this.target = Object.freeze(init.target);

    }
}