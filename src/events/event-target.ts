
interface EventListenerObject<E extends Event = Event> {
    once?: boolean
    passive?: boolean
    callback: (e: E) => boolean | void
}

export class EventTarget {
    
    private _listeners = new Map<string, EventListenerObject[]>();

    public addEventListener(type: string, callback: string, options?: any): void {

    }
    public removeEventListener(type: string, callback: string): void {

    }

    public dispatchEvent(event: Event): void {
        
        if (event.type in this._listeners) {

            const listeners = this._listeners[event.type];

            for (const listener of listeners) {



            }

        }

    }

}